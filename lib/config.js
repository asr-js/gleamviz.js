"use strict";

const zip = require("./zip").zip;

class Config {
    constructor() {
        this._webworkerPath = ".";
        this._serverURL = "https://brunate.isi.it";
    }

    get webworkerPath() {
        return this._webworkerPath;
    }

    set webworkerPath(path) {
        zip.workerScriptsPath = path;
        this._webworkerPath = path;
    }

    get serverURL() {
        return this._serverURL;
    }

    set serverURL(url) {
        this._serverURL = url;
    }
}

module.exports = new Config;

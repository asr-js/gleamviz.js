# gleamviz.js #

JavaScript library for communicating with the GLEAMviz Proxy Server

### How to build the library? ###
1. Run *npm install*
2. Run *npm run build* or *npm run watch*

### Running the example ###
A more comprehensive example can be found in the example folder:

1. Run *npm start* to start a web server at http://127.0.0.1:9999/example

### Setup the library ###
1. Include all files from the dist folder in your HTML document
```
#!html
<script src="dist/gleamviz.js"></script>
```
2. Examples
```
#!javascript
GleamViz.login("username", "password").then(userdata => {
	GleamViz.listall().then(simulations => {
		// simulations contains a list of all simulation informations
		GleamViz.logout();
	});
});

GleamViz.login("username", "password").then(userdata => {
	GleamViz.getsimulationdata("1486985747939.ASR")
		.then(data => {
			// data contains the simulation data
			GleamViz.logout();
		});
});

GleamViz.login("username", "password").then(userdata => {
	GleamViz.create(definition)
		.then(data => {
			GleamViz.logout();
		});
});
```
### API ###

```
#!javascript
GleamViz.login(username, password)
  .then(userdata)
  .catch(error)
  
GleamViz.logout(username, password)
  .then()
  .catch(error)
  
GleamViz.listall(username, password)
  .then(simulations)
  .catch(error)

GleamViz.create(definition)
  .then()
  .catch(error)
  
GleamViz.remove(id)
  .then()
  .catch(error)
  
GleamViz.stop(id)
  .then()
  .catch(error)
  
GleamViz.getinfo(id)
  .then(info)
  .catch(error)
  
GleamViz.getdef(id)
  .then(definition)
  .catch(error)
  
GleamViz.getabmdata(id)
  .then(data)
  .catch(error)
  
GleamViz.getdata(id)
  .then(data)
  .catch(error)
  
GleamViz.getparameters(id)
  .then(parameters)
  .catch(error)
  
GleamViz.getsimulationdata(id)
  .then(simulation)
  .catch(error)
```



## Authors

Authors of this project (comprising ideas, architecture, and code) are:

* Sebastian Alberternst <sebastian.alberternst@dfki.de>
* Jan Sutter <jan.sutter@dfki.de>

This project and code was mainly developed by:

* [DFKI](https://www.dfki.de/web/research/asr/index_html) - German Research Center for Artificial Intelligence
* [ISI](http://www.gleamviz.org/) - Fondazione Istituto per l'Interscambio Scientifico

Parts of the project and code were developed as part of the [EU H2020](https://ec.europa.eu/programmes/horizon2020/) [project](https://www.cimplex-project.eu/) *CIMPLEX* - Bringing *CI*tizens, *M*odels and Data together in *P*articipatory, Interactive Socia*L* *EX*ploratories.

Futher partners that deliver data and simulations via webservice access are:

* ETHZ (ETH Zurich)
* UCL (University College of London)
* Közép-európai Egyetem (Central European University, CEU)
* CNR (National Research Council)
* FBK (Bruno Kessler Foundation)
* USTUTT (University of Stuttgart, Institute for Visualization and Interactive Systems)

## License

See [LICENSE](./LICENSE).
